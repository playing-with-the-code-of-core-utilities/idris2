module Main

import Prelude.Show
    -- show
import System
    -- getArgs
import System.File
    -- readFile

main : IO ()
main = do
    args <- getArgs
    case args of
        [] => putStrLn ("This should not happens.")
        (a0 :: []) => putStrLn ("You called: " ++ a0 ++ "\nUsage: first arg is used, others are ignored")
        (_ :: (fname :: _)) => do
            text <- readFile fname
            case text of
                Left err => printLn $ show err
                Right content => printLn content
